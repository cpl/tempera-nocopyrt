<?php

// partial template for the default header of cpl.org

?>
<div class="l-nav-container l-nav-container--default">
			<div class="l-contained">
				<div class="l-nav">
					<a class="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i></a>
					<nav role="navigation">
						<ul class="menu">
									<li class="menu-item active--default"><a href="<?php echo esc_url( get_site_url() ); ?>"><span>the</span> library</a>
								<?php
								// the main menu

									wp_nav_menu(
										array(
											'menu'       => 'home',
											'menu_class' => 'menu-primary--default',
											'theme_location' => 'primary',
										)
									);
									?>
									</li>
									<li class="menu-item"><a href="https://courbanize.com/collections/cpl"><span>Our Future</span>Is Building</a></li>
						</ul>
					</nav>
				</div> <!-- l-nav -->

			</div>  <!-- l-contained -->

			<?php get_template_part( 'templates/partials/header', 'search-button' ); ?>
		</div>  <!-- l-nav-container -->

