<?php
/**
 * The CPL search form exclusively for post_types of cpl_agenda
 *
 */
// create unique integer to create labels for
$search_unique_id = esc_attr( wp_unique_id() );
?>


<form role="search" method="GET" class="form-search"
action="<?php echo esc_url( home_url( '/' ) ); ?>">
<label for="search-input-<?php echo ( $search_unique_id ); ?>" class="screen-reader-text">Enter what you want to search for:</label>
<input type="search" id="search-input-<?php echo ( $search_unique_id ); ?>" required class="search-field" placeholder="<?php echo '" name="s"'; ?> />
<input type="hidden" value="cpl_agenda" name="post_type" id="id-post-type" />
<input class="cpl-button cpl-button-action--search" value="search" name="search" aria-label="Submit your search query" alt="Search" type="submit" />
</form>
