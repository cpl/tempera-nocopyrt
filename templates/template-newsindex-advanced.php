<?php /*
 * Template Name: Newsindex - advanced searching
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */


session_start();
include '../../lockbox/newsIndex.inc';
include '../../lockbox/web_functions.inc';
get_header();

if (!isset($_SESSION['start']) || $_SESSION['start'] === 0) {
    initSession();
}


if (!validateSession()) {
    initSession();
}

?>

<section id="container" class="<?php echo tempera_get_layout_class(); ?>">
<div id="content" role="main">
<?php
get_template_part( 'templates/partials/form', 'search-newsindex' );
?>

<div class="entry-content">


<p>There are some advanced searching techniques available to search the newsindex database.  You can use the asterisk (*) as a wild card.  To do this just add the asterisk to the end of the string you are using, for example, acke*.</p>

<p>If you want to limit your results you can use the minus (-) sign.  A search using this technique would look look like this:</p>

<p><code>ackel -andrew </code></p>

<p>Would result in all of the names with ackel being returned, except those that had andrew in them.</p>


<p>You can even combine both methods.  To look for any name that began with acke but does not use the name andrew you would type:</p>

<p><code>acke* -andrew</code></p>

</div>
</div>

<?php tempera_get_sidebar(); ?>
</section><!-- #container -->

<?php get_footer(); ?>



