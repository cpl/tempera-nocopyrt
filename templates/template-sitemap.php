<?php
/*
 * Template Name: Site map
 *
 * Based on page.php of the parent temep
 * @subpackage tempera
 * @since tempera 0.5
 */
get_header();
	?>
		<section id="container" class="<?php echo tempera_get_layout_class(); ?>">

			<div id="content" role="main">
				<?php get_template_part( 'content/content', 'sitemap' ); ?>

			<?php cryout_after_content_hook(); ?>
			</div><!-- #content -->
			<?php tempera_get_sidebar(); ?>
		</section><!-- #container -->


	<?php
get_footer();
?>
