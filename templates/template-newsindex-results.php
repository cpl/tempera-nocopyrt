<?php
/*
 * Template Name: Newsindex-results
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */
session_start();
include '../private/newsIndex.inc';
include '../private/web_functions.inc';

get_header();

if (!checkVars($sessionVars, 'session')) {
        goHome("/");
}

//check the get stuff
$checkGetVars = array(
  'id',
    'type'
);



if (!validateSession()) {
    goHome();
}

$_SESSION['searches']++;


//search time


$split = preg_split("/[\s,]/", $_GET['searchFor']);

$search = '';

foreach ($split as $s) {
    $search .= "+" . $s . ' ';
}




?>

<section id="container" class="<?php echo tempera_get_layout_class(); ?>">
<div id="content" role="main">


<?php
get_template_part( 'templates/partials/form', 'search-newsindex' );
?>
<h1 class="entry-title"> Results for <?php echo sanitize_text_field($_GET['searchFor']); ?>: </h1>
<div class="entry-content">

<?php
$dbh = dbConnect($user, $pass, $host, $db);

if ($_GET['searchType'] == 'necrology') {
    searchNecrology($dbh, $search);
}
elseif ($_GET['searchType'] == 'newsindex') {
    searchNews($dbh, $search);
}
elseif ($_GET['searchType'] == 'both') {
    $necrologyResults = searchNecrology($dbh, $search, $both=True);

    $newsResults = searchNews($dbh, $search, $both=True);

    $both = array_merge($necrologyResults, $newsResults);
	if ( ! empty( $both ) ) {
        natcasesort($both);
        echo "<ul>";
        foreach ($both as $b) {
            $parts = explode('|', $b);
            $url = '<li><a href="../showrecord?record=' . $parts[2];
            $url .= '&type=' . $parts[1];
            $url .= '&searchType=' . $_GET['searchType'] . '">';
            $url .= $parts[0] . '</a></li>';
            echo $url;
        }
        echo "</ul>";
    }
    else {
        echo '<p>No results found please adjust your search.</p>';
    }
}
else {
    goHome("/");
}


$dbh = null;

function searchNecrology($dbh, $search, $both=False) {
    $query = "select id, lastName, firstName from necrology_new where match(lastName, firstName, notes) against (:searchstr IN Boolean MODE) order by lastName, firstName";

    if ($both == True) {
        $necrologyResults = array();
    }


    try {
        $sth = $dbh->prepare($query);
        $sth->bindParam(':searchstr', $search);
        $sth->execute();
        $i = 0;
        if ($both == False) {
            echo '<ul>';
        }
        while ($row = $sth->fetch(PDO::FETCH_BOTH)) {
            $itemID = urlencode($row[0]);

            if ($both == False) {
                $s = '<li><a href="../showrecord?';
                $s .= 'record=' . $itemID . '&type=necrology&searchType=' . $_GET['searchType'] . '">';
                $s .= $row[2] . " " . $row[1] . "</a></li>\n";
                echo $s;
                $i ++;
            }
            else {
                $necrologyResults[] = $row[1] . ', ' . $row[2] . '|' . 'necrology' . '|' . $itemID;
            }
        }
        if ($both == False) {
            echo '</ul>';
            if ($i === 0) {
                echo "<p>No results found please adjust your search.</p>";
            }
        }
        else {
            return $necrologyResults;
        }
    }
    catch (Exception $e) {
        $dbError = $e->getMessage();
        sendMessage($message=$dbError, "Database error - results.php - search necrology", $to='website@cpl.org');
        echo "<p>We are having technical difficulties.  Please try again later.</p>";
        exit;
    }
    return True;
}



function searchNews($dbh, $search, $both=False) {
    $query = "select subjectID, subject from subjects where match(subject) against (:searchstr IN Boolean MODE) order by subject";
    if ($both == True) {
        $newsResults = array();
    }

    try {

        $sth = $dbh->prepare($query);
        $sth->bindParam(':searchstr', $search);
        $sth->execute();
        $i = 0;
        if ($both == False) {
            echo '<ul>';
        }
        while ($row = $sth->fetch(PDO::FETCH_BOTH)) {
            $itemID = urlencode($row[0]);
            if ($both == False) {

                $s = '<li><a href="../showrecord?';
                $s .= 'record=' . $itemID . '&type=news&searchType=' . $_GET['searchType'] . '">';
                $s .= ucwords(strtolower($row[1])) . "</a></li>\n";
                echo $s;
            }
            else {
                $newsResults[] =  ucwords(strtolower($row[1])) . '|' . 'news' . '|' . $itemID;
            }
            $i ++;
        }

        if ($both == False) {
            echo '</ul>';
            if ($i === 0) {
                echo "<p>No results found please adjust your search.</p>";
            }
        }
        else {
            return $newsResults;
        }
    }
    catch (Exception $e) {
        $dbError = $e->getMessage();
        sendMessage($message=$dbError, "Database error - results.php - search news", $to='website@cpl.org');
        echo "<p>We are having technical difficulties.  Please try again later.</p>";
        exit;
    }
    return False;
}

?>
</div><!-- #content -->
</div><!-- #entry-content -->
<?php
tempera_get_sidebar(); ?>
</section><!-- #container -->

<?php get_footer(); ?>
