wp.domReady(function () {
  console.log("linda eastman time!");
  const allowedEmbeds = [
    "facebook",
    "instagram",
    "twitter",
    "vimeo",
    "youtube",
  ];

  // for each object (a different embed varation) in the wp.blocks.getBlockVariations("core/embed") array,
  // check if its name equals a value in the allowEmbeds array; if false, unregisterblockvariation
  //
  wp.blocks.getBlockVariations("core/embed").forEach(function (embedVariation) {
    if (allowedEmbeds.includes(embedVariation.name) === false) {
      wp.blocks.unregisterBlockVariation("core/embed", embedVariation.name);
    }
  });
});
