<?php

add_action( 'init', 'register_cpl_block_pattern_categories' );

function register_cpl_block_pattern_categories() {
	register_block_pattern_category(
		'alert',
		array( 'label' => __( 'Alerts', 'tempera-nocopyrt' ) )
	);
}
register_block_pattern(
	'tempera-nocopyrt/alert-informational',
	array(
		'title'       => __( 'Alert - Informational', 'tempera-nocopyrt' ),
		'description' => _x( 'informational alert', 'tempera-nocopyrt' ),
		'categories'  => array( 'alert' ),
		'keywords'    => 'alert',
		'inserter'    => true,
		'content'     => '<!-- wp:group {"className":"cpl-alert cpl-alert\u002d\u002dinfo"} -->
		<div class="wp-block-group cpl-alert cpl-alert--info"><!-- wp:group {"className":"cpl-alert__body"} -->
		<div class="wp-block-group cpl-alert__body"><!-- wp:heading {"className":"cpl-alert__heading"} -->
		<h2 class="cpl-alert__heading">Informational Heading</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph {"className":"cpl-alert__text"} -->
		<p class="cpl-alert__text">Edit a sentence or two here</p>
		<!-- /wp:paragraph --></div>
		<!-- /wp:group --></div>
		<!-- /wp:group -->',
	)
);

register_block_pattern(
	'tempera-nocopyrt/alert-success',
	array(
		'title'       => __( 'Alert - Success', 'tempera-nocopyrt' ),
		'description' => _x( 'informational alert', 'tempera-nocopyrt' ),
		'categories'  => array( 'alert' ),
		'keywords'    => 'alert',
		'inserter'    => true,
		'content'     => '<!-- wp:group {"className":"cpl-alert cpl-alert\u002d\u002dsuccess"} -->
		<div class="wp-block-group cpl-alert cpl-alert--success"><!-- wp:group {"className":"cpl-alert__body"} -->
		<div class="wp-block-group cpl-alert__body"><!-- wp:heading {"className":"cpl-alert__heading"} -->
		<h2 class="cpl-alert__heading">Success Heading</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph {"className":"cpl-alert__text"} -->
		<p class="cpl-alert__text">Edit a sentence or two here</p>
		<!-- /wp:paragraph --></div>
		<!-- /wp:group --></div>
		<!-- /wp:group -->',

	)
);

register_block_pattern(
	'tempera-nocopyrt/storytime-facebook',
	array(
		'title'       => __( 'Storytime - Facebook', 'tempera-nocopyrt' ),
		'description' => _x( 'Storytime with Facebook embed', 'tempera-nocopyrt' ),
		'categories'  => array( 'buttons', 'featured', 'text' ),
		'keywords'    => 'storytime',
		'inserter'    => true,
		'content'     => '<!-- wp:html -->
		<div id="fb-root"></div>
		<script async="" defer="" crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v6.0"></script>
		<!-- /wp:html -->

		<!-- wp:paragraph -->
		<p>Thank you, intro sentence.</p>
		<!-- /wp:paragraph -->

		<!-- wp:heading -->
		<h2 id="block-e6fe98e3-9844-4cc4-b49a-a0d8ce27d45e"><em>The Title</em> by Author</h2>
		<!-- /wp:heading -->

		<!-- wp:html /-->

		<!-- wp:spacer {"height":"20px"} -->
		<div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
		<!-- /wp:spacer -->

		<!-- wp:media-text {"mediaId":20583,"mediaType":"image","className":"alignwide"} -->
		<div class="wp-block-media-text alignwide is-stacked-on-mobile"><figure class="wp-block-media-text__media"><img src="https://cpl.org/wp-content/uploads/what-if.jpg" alt="" class="wp-image-20583 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"level":3} -->
		<h3 id="block-cff04fcb-cf7e-497b-8547-41c39aadbc4c">About <em>The title</em></h3>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p> Ensure the image ratio is 1x1.</p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Stunning mixed media illustrations, lyrical text, and a breathtaking gatefold conjure powerful magic in this heartfelt affirmation of art, imagination, and the resilience of the human spirit.</p>
		<!-- /wp:paragraph -->

		<!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-cpl-button\u002d\u002daction"} -->
		<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link" href="https://search.clevnet.org/client/en_US/clevnet/search/results?qu=&amp;qu=TITLE%3DWhat+if+&amp;qu=AUTHOR%3DSamantha+Berger+&amp;h=1">ADD TITLE TO MY LIST</a></div>
		<!-- /wp:button --></div>
		<!-- /wp:buttons --></div></div>
		<!-- /wp:media-text -->',

	)
);

register_block_pattern(
	'tempera-nocopyrt/storytime-youtube',
	array(
		'title'       => __( 'Storytime - youtube', 'tempera-nocopyrt' ),
		'description' => _x( 'Storytime with youtube embed', 'tempera-nocopyrt' ),
		'categories'  => array( 'buttons', 'featured', 'text' ),
		'keywords'    => 'storytime',
		'inserter'    => true,
		'content'     => '<!-- wp:paragraph -->
		<p>News 5 Cleveland\'s Danita Harris reads <em>Like A Girl</em> by Lori Degman for storytime. </p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Visit a branch near you: <a href="https://cpl.org/locations">https://cpl.org/locations</a></p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Watch: <a href="https://cpl.org/virtual-storytimes">https://cpl.org/virtual-storytimes</a></p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Listen to Dial-A-Story: <a href="tel:216-777-6500">216-777-6500</a></p>
		<!-- /wp:paragraph -->

		<!-- wp:embed {"url":"https://youtu.be/YOP3v1OS2PM","type":"video","providerNameSlug":"youtube","responsive":true,"className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->
		<figure class="wp-block-embed is-type-video is-provider-youtube wp-block-embed-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio"><div class="wp-block-embed__wrapper">
		https://youtu.be/YOP3v1OS2PM
		</div></figure>
		<!-- /wp:embed -->

		<!-- wp:spacer {"height":"20px"} -->
		<div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
		<!-- /wp:spacer -->

		<!-- wp:spacer {"height":"20px"} -->
		<div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
		<!-- /wp:spacer -->

		<!-- wp:media-text {"mediaId":31009,"mediaLink":"https://cpl.org/?attachment_id=31009","mediaType":"image","mediaWidth":25,"isStackedOnMobile":false,"verticalAlignment":"center"} -->
		<div class="wp-block-media-text alignwide is-vertically-aligned-center" style="grid-template-columns:25% auto"><figure class="wp-block-media-text__media"><img src="https://cpl.org/wp-content/uploads/like-a-girl.jpg" alt="" class="wp-image-31009 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"level":3} -->
		<h3 id="about-like-a-girl-by-lori-degman">About <em>Title</em> by Author</h3>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p>Please ensure the image ratio is 1x1. Once upon a time, "like a girl" was considered an insult. Not anymore! In art, aviation, politics, sports, every walk of life, girls are demonstrating their creativity, perseverance, and strength. From civil rights activist Rosa Parks, who stood up for her beliefs by staying seated, to astronaut Sally Ride, who soared to the skies, the 24 women profiled here took risks, acted up, broke barriers, and transformed the world. With its simple yet powerful text, this book will inspire young women everywhere. Want more stories like this?</p>
		<!-- /wp:paragraph -->

		<!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"sec-goldenrod","textColor":"very-dark-gray","className":"is-style-cpl-button\u002d\u002daction"} -->
		<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link has-very-dark-gray-color has-sec-goldenrod-background-color has-text-color has-background" href="https://search.clevnet.org/client/en_US/clevnet/search/detailnonmodal/ent:$002f$002fSD_ILS$002f0$002fSD_ILS:7740732/one?qu=Like+a+Girl&amp;qu=-&amp;qu=Lori+Degman"><strong>Add title to My List</strong></a></div>
		<!-- /wp:button --></div>
		<!-- /wp:buttons --></div></div>
		<!-- /wp:media-text -->

		<!-- wp:block {"ref":17850} /-->',
	)
);

register_block_pattern(
	'tempera-nocopyrt/landing-page',
	array(
		'title'       => __( 'landing page', 'tempera-nocopyrt' ),
		'description' => _x( 'landing page', 'tempera-nocopyrt' ),
		'categories'  => array( 'buttons', 'featured', 'text' ),
		'keywords'    => 'landing',
		'inserter'    => true,
		'content'     => '<!-- wp:image {"id":27510,"sizeSlug":"large","linkDestination":"media","className":"is-style-default"} -->
		<figure class="wp-block-image size-large is-style-default"><a href="https://cpl.org/wp-content/uploads/legal-header.jpg"><img src="https://cpl.org/wp-content/uploads/legal-header-1024x341.jpg" alt="" class="wp-image-27510"/></a></figure>
		<!-- /wp:image -->

		<!-- wp:spacer {"height":"50px"} -->
		<div style="height:50px" aria-hidden="true" class="wp-block-spacer"></div>
		<!-- /wp:spacer -->

		<!-- wp:media-text {"mediaId":23671,"mediaLink":"https://cpl.org/services/download-3/","mediaType":"image","mediaWidth":25,"isStackedOnMobile":false} -->
		<div class="wp-block-media-text alignwide" style="grid-template-columns:25% auto"><figure class="wp-block-media-text__media"><img src="https://cpl.org/wp-content/uploads/download.jfif" alt="" class="wp-image-23671 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading -->
		<h2>Civil Assitance with Legal Aid</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p>The Legal Aid Society offers free advice clinics for individuals with civil legal issues on a first come, first served basis. Legal Aid Society does not handle criminal cases or traffic violations. Please bring all relevant paperwork with you.</p>
		<!-- /wp:paragraph -->

		<!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-cpl-button\u002d\u002daction"} -->
		<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link" href="https://lasclev.org/?s=cleveland+public+library&amp;x=0&amp;y=0">upcoming workshops</a></div>
		<!-- /wp:button --></div>
		<!-- /wp:buttons --></div></div>
		<!-- /wp:media-text -->

		<!-- wp:separator {"opacity":"css","className":"is-style-default"} -->
		<hr class="wp-block-separator has-css-opacity is-style-default"/>
		<!-- /wp:separator -->

		<!-- wp:media-text {"mediaId":27509,"mediaLink":"https://cpl.org/?attachment_id=27509","mediaType":"image","mediaWidth":25,"isStackedOnMobile":false} -->
		<div class="wp-block-media-text alignwide" style="grid-template-columns:25% auto"><figure class="wp-block-media-text__media"><img src="https://cpl.org/wp-content/uploads/legal-works.jpg" alt="" class="wp-image-27509 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading -->
		<h2>LegalWorks at Cleveland Public Library</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p>LegalWorks is a non-profit organization that provides legal consultation and assistance for qualified, low-income youth and adults in underserved communities.</p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Areas of assistance focus on legal issues that prevent people from moving forward in their lives, including expungements, sealing records, obtaining certificates of qualified employment, clearing outstanding warrants, restoring driving privileges, and other select legal matters. </p>
		<!-- /wp:paragraph -->

		<!-- wp:group -->
		<div class="wp-block-group"><!-- wp:paragraph -->
		<p><strong><em>Registration is required.</em><br><br>Fulton Branch</strong> | <strong>Tuesdays | 12 – 3:30PM</strong><br>3545 Fulton Rd. | 216-623-6969</p>
		<!-- /wp:paragraph -->

		<!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-cpl-button\u002d\u002daction"} -->
		<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link" href="https://www.eventbrite.com/e/legalworks-at-cleveland-public-library-fulton-branch-tickets-148043936399">REGISTER FOR FULTON</a></div>
		<!-- /wp:button --></div>
		<!-- /wp:buttons --></div>
		<!-- /wp:group -->

		<!-- wp:paragraph -->
		<p><br><strong>Mt. Pleasant Branch</strong> | <strong>Wednesdays | 12 – 3:30PM</strong><br>14000 Kinsman Rd. | 216-623-7032</p>
		<!-- /wp:paragraph -->

		<!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-cpl-button\u002d\u002daction"} -->
		<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link" href="https://www.eventbrite.com/e/legalworks-at-cleveland-public-library-mt-pleasant-tickets-148076094585">REGISTER FOR MT. PLEASANT</a></div>
		<!-- /wp:button --></div>
		<!-- /wp:buttons -->

		<!-- wp:paragraph -->
		<p>Need legal advice or help now?&nbsp; Need an appointment on a different day or time? Visit<strong> <a href="http://legalworksneo.org">legalworksneo.org</a> </strong>for more information.</p>
		<!-- /wp:paragraph --></div></div>
		<!-- /wp:media-text -->

		<!-- wp:block {"ref":17850} /-->',
	)
);

register_block_pattern(
	'tempera-nocopyrt/research-aid-template',
	array(
		'title'       => __( 'Research Aid Template', 'tempera-nocopyrt' ),
		'description' => _x( 'Research Aid Template', 'tempera-nocopyrt' ),
		'categories'  => array( 'buttons', 'featured', 'text' ),
		'keywords'    => 'research',
		'inserter'    => true,
		'content'     => '<!-- wp:paragraph -->
		<p> 3-4 sentence introduction about the neighborhood. This field cna be edited</p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Need additional assistance? For more information, please contact our <a href="https://cpl.org/aboutthelibrary/subjectscollections/center-for-local-and-global-history/">Center for Local and Global History</a> </p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>Phone: <a href="tel:1-216-623-2864">216-623-2864</a>     Email: <a href="mailto:clgh@cpl.org">clgh@cpl.org</a></p>
		<!-- /wp:paragraph -->

		<!-- wp:heading -->
		<h2>Books</h2>
		<!-- /wp:heading -->


		<!-- wp:heading -->
		<h2>Maps</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p><strong>For more information about neighborhood maps, please contact the </strong><a rel="noreferrer noopener" href="https://cpl.org/aboutthelibrary/subjectscollections/map-collection/" target="_blank"><strong>Map Collection</strong></a><strong>.</strong>&nbsp;</p>
		<!-- /wp:paragraph -->


		<!-- wp:heading -->
		<h2>Microfilm/Microfiche</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p>Microfilm resources may include newspaper clippings, city reports, and other historical documents. Depending on the scope of the work, these items may be available in several departments, including History, the <a href="https://cpl.org/aboutthelibrary/subjectscollections/popular-library/">Microform Center</a>, or <a href="https://cpl.org/locations/public-administration-library/">Public Administration Library</a>.</p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p><strong>Center for Local and Global History:</strong>&nbsp;</p>
		<!-- /wp:paragraph -->



		<!-- wp:heading -->
		<h2>Photographs</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p><strong>For more information about the neighborhood photos, please contact the <a href="https://cpl.org/aboutthelibrary/subjectscollections/photograph-collection/">Photograph Collection</a>.</strong> </p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>The Photograph Collection has 1.3 million items, including a large collection of historical images for Cleveland and northeast Ohio. A small portion of them are available in our digital collection. </p>
		<!-- /wp:paragraph -->

		<!-- wp:paragraph -->
		<p>For specific photo requests, please think about important streets, intersections, institutions like schools, businesses, churches, and other important landmarks. This will help aid in our research.</p>
		<!-- /wp:paragraph -->

		<!-- wp:buttons -->
		<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-cpl-button\u002d\u002daction"} -->
		<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link" href="otherurl.com">View more photos in our digital collection</a></div>
		<!-- /wp:button --></div>
		<!-- /wp:buttons -->

		<!-- wp:heading -->
		<h2>Yearbooks</h2>
		<!-- /wp:heading -->

		<!-- wp:paragraph -->
		<p><strong>For more information about yearbooks at the library, please contact the </strong><a rel="noreferrer noopener" href="https://cpl.org/aboutthelibrary/subjectscollections/social-sciences/" target="_blank"><strong>Social Sciences Department</strong></a><strong>.</strong>&nbsp;</p>
		<!-- /wp:paragraph -->
		',
	)
);
