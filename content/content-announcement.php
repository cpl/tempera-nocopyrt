<?php

// announcement compontent query
$announcement_args = array(
	'post_type'      => 'announcements',
	'post_status'    => 'publish',
	'posts_per_page' => '1',
);
?>

<?php
// if on the frontpage, use h1, else, use an h2

	$announcement_loop = new WP_Query( $announcement_args );
	if ( have_posts() ) :
		while ( $announcement_loop->have_posts() ) :
			$announcement_loop->the_post(); ?>
			<div class="cpl-announcement" role="alert" >
			<?php the_content(); ?>
			</div>
			<?php
			endwhile;
		endif;
?>
