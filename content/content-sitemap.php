<?php
/**
 * The default template for displaying content
 *
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.0
 */


if ( have_posts() ) {
	while ( have_posts() ) :
		the_post();
		cryout_before_content_hook(); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1 class="entry-title"><?php the_title(); ?></h1>

	<div class="entry-content">
			<?php
			wp_list_pages(
				array(
					'title_li'    => 'Pages',
					'sort_column' => 'post_title',
					'post_status' => 'publish',
					'post_type'   => 'page',
				)
			);
			?>
		<div style="clear:both;"></div>
			<?php
			wp_link_pages(
				array(
					'before' => '<div class="page-link">' . __( 'Pages:', 'tempera' ),
					'after'  => '</div>',
				)
			);
			?>
		<?php edit_post_link( __( 'Edit', 'tempera' ), '<span class="edit-link"><i class="crycon-edit"></i> ', '</span>' ); ?>
	</div><!-- .entry-content -->
</div><!-- #post-## -->

		<?php
endwhile;
};
?>
